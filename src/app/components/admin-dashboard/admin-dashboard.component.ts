import { AuthServiceService } from './../../services/auth-service.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { DataServiceService } from 'src/app/services/data-service.service';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { Router } from '@angular/router';
import { ScrollingModule } from '@angular/cdk/scrolling';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  posts: any;
  error: any;
  complaintsData: any;
  agentName: string;
  bonusPoints: number;


  /** Based on the screen size, switch from standard to one column per row */
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { title: 'Posts Added', cols: 1, rows: 1 },
          { title: 'Assign Request', cols: 1, rows: 1 },
          { title: 'Complaint Box', cols: 1, rows: 1 },
          { title: 'Add Bonus', cols: 1, rows: 1 }
        ];
      }

      return [
        { title: 'Posts Added', cols: 2, rows: 1 },
        { title: 'Assign Request', cols: 1, rows: 1 },
        { title: 'Complaint Box', cols: 1, rows: 2 },
        { title: 'Add Bonus', cols: 1, rows: 1 }
      ];
    })
  );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private dataService: DataServiceService,
    private spinner: NgxUiLoaderService,
    private authService: AuthServiceService
    ) {}

    ngOnInit(){
      this.getPostsData();
      this.getComplaints();
    }
    getPostsData(){
      this.spinner.start();
      this.dataService.getAllPosts().subscribe((response)=>{
        console.log(response);
        if(response.success){
          console.log(response.posts);
          this.posts = response.posts;
          this.spinner.stop();
        } else {
          this.error = response.msg;
        }
      })
    }
  onEditClick(postId) {
    this.router.navigate([`/edit-post/${postId}`]);
  }
  onDeleteClick(postId) {
    this.spinner.start();
    this.dataService.deletePostById(postId).subscribe((response: any) => {
      console.log(response);
      if (response.success) {
        this.router.navigate(['/posts']);
        this.spinner.stop();
        window.location.reload();
      }
    })
  }

  getComplaints(){
    this.spinner.start();
    this.authService.getComplaintsData().subscribe((response: any)=>{
      if(response.success){
        this.complaintsData = response.complaints;
        console.log(this.complaintsData)
        this.spinner.stop();
      }
    })
  }
  
  onAddBonusClick(){
    this.spinner.start();
    const bonusInfo = {
      name: this.agentName,
      bonus: this.bonusPoints
    };
    this.dataService.addBonusPoints(bonusInfo).subscribe((response: any)=>{
      console.log(response);
      if(response.success){
        this.spinner.stop();
      } else {
        this.error = response.msg;
      }
    });
  }

}
