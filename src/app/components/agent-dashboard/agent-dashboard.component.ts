import { AuthServiceService } from './../../services/auth-service.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { DataServiceService } from './../../services/data-service.service';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { Router } from '@angular/router';

@Component({
  selector: 'app-agent-dashboard',
  templateUrl: './agent-dashboard.component.html',
  styleUrls: ['./agent-dashboard.component.css']
})
export class AgentDashboardComponent implements OnInit {
  agentData: any;
  error: string;
  showComplaintBox: boolean = false;
  title: string;
  description: string;
  msg: string;
  pointsEarned: number;
  
  /** Based on the screen size, switch from standard to one column per row */
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          // { title: 'Card 1', cols: 1, rows: 1 },
          { title: 'Assign Request', cols: 1, rows: 1 },
          { title: 'Options', cols: 1, rows: 1 },
          { title: 'Points Earned', cols: 1, rows: 1 }
        ];
      }

      return [
        // { title: 'Card 1', cols: 2, rows: 1 },
        { title: 'Assign Request', cols: 1, rows: 1 },
        { title: 'Options', cols: 1, rows: 2 },
        { title: 'Points Earned', cols: 1, rows: 1 }
      ];
    })
  );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private dataService: DataServiceService,
    private router: Router,
    private spinner: NgxUiLoaderService,
    private authService: AuthServiceService
    ) {}

    ngOnInit(){
      this.getAgentDashboard();
      this.getPointsEarned();
    }

    getAgentDashboard(){
      this.spinner.start();
      this.dataService.getAgentsDashboardData().subscribe((response: any) => {
        if (response.success) {
          this.agentData = response.user;
          console.log(this.agentData)
          this.spinner.stop();
        } else {
          this.error = response.msg;
        }
      })
    }
    getPointsEarned(){
      this.spinner.start();
      this.dataService.getBonusPoints().subscribe((response: any)=>{
        console.log(response);
        if(response.success){
          this.spinner.stop();
          this.pointsEarned = response.points;
        } else {
          this.error = response.msg;
        }
      })
    }
    addComplaint(){
      this.showComplaintBox = !this.showComplaintBox;
      console.log('add complaint clicked');
    }
    onSendClick(){
      const complaintObject = {
        title: this.title,
        description: this.description
      };
      console.log(complaintObject);
      this.spinner.start();
      this.authService.registerComplaint(complaintObject).subscribe((response: any)=>{
        console.log(response);
        if(response.success){
          this.spinner.stop();
          this.msg = 'Your complaint is successfully registered ';
        } else {
          this.error = response.msg;
        }
      });
    }
}
