import { Router } from '@angular/router';
import { DataServiceService } from 'src/app/services/data-service.service';
import { Component, OnInit } from '@angular/core';
import { NgxUiLoaderService } from "ngx-ui-loader";

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})

export class CreatePostComponent implements OnInit {
  title: string;
  description: string;
  points: number;
  error: string;

  constructor(
    private dataService: DataServiceService,
    private router: Router,
    private spinner: NgxUiLoaderService
  ) { }

  ngOnInit() {
  }

  onFormSubmit(){
    const postInfo = {
      title: this.title,
      description: this.description,
      points: this.points
    };
    this.spinner.start();
    this.dataService.createPost(postInfo).subscribe((response)=>{
      console.log(response);
      if(response.success){
        console.log(response.msg);
        this.spinner.stop();
        this.router.navigate(['/posts']);
      } else {
        this.error = response.msg;
      }
    })
  }

}
