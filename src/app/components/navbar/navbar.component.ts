import { Component, OnInit, OnChanges, AfterViewInit } from '@angular/core';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
admin: Boolean;

  constructor(
    public authService: AuthServiceService,
    private router: Router
  ) { }

  ngOnInit() {
    this.authService.updateAdminData.subscribe((res)=>{
      if(res){
        this.authService.adminOrAgent.subscribe((response) => {
          console.log(response);
          this.admin = response;
        })
      }
    })
  }
  ngOnChanges(){
    this.admin = this.authService.getAdmin();
    console.log(this.admin)
  }

  onLogoutClick(){
    this.authService.logout();
    this.router.navigate(['/login'])
  }
}
