import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { Router } from '@angular/router';

@Component({
  selector: "app-sign-in",
  templateUrl: "./sign-in.component.html",
  styleUrls: ["./sign-in.component.css"]
})
export class SignInComponent implements OnInit {
  email: string;
  password: string;
  error: string;
  adminOrAgent: boolean;

  constructor(
    public authService: AuthServiceService,
    public router: Router
    ) {}

  ngOnInit() {}
  onLoginSubmit(){
    const loginInfo = {
      email: this.email,
      password: this.password
    };
    this.authService.login(loginInfo).subscribe((response: any)=>{
      if(response.success){
        console.log(response);
        this.authService.storeUser(response.token, response.admin);
        this.router.navigate(['/posts']);
      } else {
        this.error = response.msg;
      }
    });
  }
}
