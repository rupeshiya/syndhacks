import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { DataServiceService } from 'src/app/services/data-service.service';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from "ngx-ui-loader";

@Component({
  selector: "app-posts",
  templateUrl: "./posts.component.html",
  styleUrls: ["./posts.component.css"]
})
export class PostsComponent implements OnInit {
  posts = new Array();
  error: string;
  commentText: string;
  admin: boolean;
  showComments: boolean;
  addComment: boolean;

  constructor(
    private authService: AuthServiceService,
    private dataService: DataServiceService,
    private router: Router,
    private spinner: NgxUiLoaderService
  ) { }

  ngOnInit() {
    this.spinner.start();
    this.admin = this.authService.getAdmin();
    console.log(this.admin);
    this.authService.updateAdmin.next(true);
    this.authService.isAdmin.next(this.admin);
    this.dataService.getAllPosts().subscribe(response => {
      if (response.success) {
        this.posts = response.posts;
        this.spinner.stop();
        console.log(this.posts);
      } else {
        this.error = response.msg;
      }
    });
  }
  onCommentClick() {
    this.addComment = !this.addComment;
  }
  onEditClick(postId){
    this.router.navigate([`/edit-post/${postId}`]);
  }
  onDeleteClick(postId){
    this.spinner.start();
    this.dataService.deletePostById(postId).subscribe((response: any  )=>{
      console.log(response);
      if(response.success){
        this.router.navigate(['/posts']);
        this.spinner.stop();
        window.location.reload();
      }
    })
  }
  onShowCommentsClick() {
    this.showComments = !this.showComments
    console.log("show comments clicked", this.showComments);
  }
  addCommentClick(postId){
    const commentInfo = {
      text: this.commentText
    };
    this.dataService.commentOnPost(commentInfo, postId).subscribe(response => {
      console.log(response);
      if (response.success) {
        console.log(response.msg);
        this.commentText = "";
        this.addComment = !this.addComment;
      } else {
        this.error = response.msg;
      }
    });
  }

}
