import { NgxUiLoaderService } from 'ngx-ui-loader';
import { DataServiceService } from './../../services/data-service.service';
import { AuthServiceService } from './../../services/auth-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { post } from 'selenium-webdriver/http';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit {
title: string;
description: string;
points: number;
postId: string;
error: string;
notRegistered: boolean = false;

  constructor(
    private router: Router,
    private authService: AuthServiceService,
    private dataService: DataServiceService,
    private activatedRoute: ActivatedRoute,
    private spinner: NgxUiLoaderService
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params)=>{
      this.postId = params.postId;
      this.getSinglePostById(params.postId);
    })
  }

  getSinglePostById(postId){
    this.spinner.start();
    this.dataService.getSinglePostById(postId).subscribe((response)=>{
      console.log(response);
      if(response.success){
        this.title = response.post.title;
        this.description = response.post.description;
        this.points = response.post.points;
        this.spinner.stop();
      }
    });
  }
  onFormSubmit(){
    const postInfo = {
      title: this.title,
      description: this.description,
      points: this.points
    };
    this.spinner.start();
    this.dataService.editPost(this.postId, postInfo).subscribe((response)=>{
      console.log(response);
      if(response.success){
        this.spinner.stop();
        this.router.navigate(['/posts']);
      } else {
        this.error = response.msg;
      }
    })
  }

}
