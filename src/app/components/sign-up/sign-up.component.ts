import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from 'src/app/services/auth-service.service';
import { Router } from '@angular/router';

@Component({
  selector: "app-sign-up",
  templateUrl: "./sign-up.component.html",
  styleUrls: ["./sign-up.component.css"]
})
export class SignUpComponent implements OnInit {
  name: string;
  email: string;
  password: string;
  notRegistered: boolean = false;
  error: string;
  adminOrAgent: boolean;

  constructor(
    public authService: AuthServiceService,
    public router: Router
    ) {}

  ngOnInit() {}
  onFormSubmit(){
    const userinfo = {
      name: this.name,
      email: this.email,
      password: this.password,
      adminOrAgent: this.adminOrAgent
    };
    this.authService.registerUser(userinfo).subscribe((response: any)=>{
      console.log(response);
      if(response.success){
        console.log(response.msg);
        this.router.navigate(['/posts']);
      } else {
        this.error = response.msg;
      }
    });
  }
  selectOption(value){
    console.log(value);
    this.adminOrAgent = value;
  }
}
