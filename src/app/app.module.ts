import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { PostsComponent } from './components/posts/posts.component';
import { HomeComponent } from './components/home/home.component';
import { AuthServiceService } from './services/auth-service.service';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http"; 
import { AuthGuardGuard } from './guards/auth-guard.guard';
import { FooterComponent } from './components/footer/footer.component';
import { EditPostComponent } from './components/edit-post/edit-post.component';
import { CreatePostComponent } from './components/create-post/create-post.component';
import { NgxUiLoaderModule } from "ngx-ui-loader";
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { LayoutModule } from '@angular/cdk/layout';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';
import { AgentDashboardComponent } from './components/agent-dashboard/agent-dashboard.component'; // Import NgxUiLoaderService
import { ScrollingModule } from '@angular/cdk/scrolling';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';


@NgModule({
  declarations: [
    AppComponent,
    SignUpComponent,
    SignInComponent,
    PostsComponent,
    HomeComponent,
    NavbarComponent,
    FooterComponent,
    EditPostComponent,
    CreatePostComponent,
    AdminDashboardComponent,
    AgentDashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxUiLoaderModule,
    BrowserAnimationsModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule,
    ScrollingModule
  ],
  providers: [
    AuthServiceService,
    HttpClientModule,
    AuthGuardGuard,
    NgxUiLoaderService,
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
