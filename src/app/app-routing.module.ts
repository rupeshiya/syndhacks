import { AgentDashboardComponent } from './components/agent-dashboard/agent-dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { PostsComponent } from './components/posts/posts.component';
import { AuthGuardGuard } from './guards/auth-guard.guard';
import { EditPostComponent } from './components/edit-post/edit-post.component';
import { CreatePostComponent } from './components/create-post/create-post.component';
import { AdminDashboardComponent } from './components/admin-dashboard/admin-dashboard.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: SignInComponent },
  { path: 'register', component: SignUpComponent },
  { path: 'home', component: HomeComponent },
  { path: 'posts', component: PostsComponent, canActivate: [AuthGuardGuard]},
  { path: 'dashboard', component: AgentDashboardComponent, canActivate: [AuthGuardGuard] },
  { path: 'edit-post/:postId', component: EditPostComponent, canActivate: [AuthGuardGuard] },
  { path: 'create-post', component: CreatePostComponent, canActivate: [AuthGuardGuard] },
  { path: 'admin-dashboard', component: AdminDashboardComponent, canActivate: [AuthGuardGuard] },
  { path: "", redirectTo: "/home", pathMatch: "full" }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
