import { AuthServiceService } from 'src/app/services/auth-service.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'syndHack';
  token: string; 
  constructor(
    private authService: AuthServiceService,
    private router: Router
  ){}
  ngOnInit(): void {
    this.token = this.authService.loggedIn();
    if(!this.token){
      this.router.navigate(['/home']);
    }
  }
}
