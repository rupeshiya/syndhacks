import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: "root"
})
export class AuthServiceService {
  baseUrl = "/api";
  authToken: string;
  user: any;
  public updateAdmin =  new BehaviorSubject(false);
  updateAdminData = this.updateAdmin.asObservable();
  
  public isAdmin = new BehaviorSubject(false);
  adminOrAgent = this.isAdmin.asObservable();

  constructor(private http: HttpClient) {}

  // register user
  registerUser(userInfo) {
    let url = this.baseUrl + "/users/register";
    const headers = new HttpHeaders();
    headers.append("Content-Type", "application/json");
    return this.http.post(url, userInfo, { headers: headers });
  }

  // login user
  login(userInfo) {
    let url = this.baseUrl + "/users/login";
    const headers = new HttpHeaders();
    headers.append("Content-Type", "application-json");
    return this.http.post(url, userInfo, { headers: headers });
  }

  // store user
  storeUser(token, adminOrAgent) {
    localStorage.setItem("token", token);
    // localStorage can only store the string so convert user object into token
    this.authToken = token;
    localStorage.setItem("admin",adminOrAgent);
  }

  // logout user
  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
    return false;
  }

  // check if logged in
  loggedIn() {
    // return tokenNotExpired('token');
    const token = localStorage.getItem("token");
    return token;
  }

  // load user data
  loadUserData() {
    const token = localStorage.getItem("token");
    this.authToken = token;
    const user = localStorage.getItem("user");
    this.user = JSON.parse(user);
  }
  // get admin 
  getAdmin(){
    const adminOrAgent = JSON.parse(localStorage.getItem("admin"));
    console.log(adminOrAgent);
    if(adminOrAgent){   
      return true;
    } else {
      return false;
    }
  }
  // add complaints 
  registerComplaint(complaintInfo){
    const url = this.baseUrl + '/users/complaint';
    const headers = new HttpHeaders({
      Authorization: `${localStorage.getItem("token")}`,
      'Content-Type': 'application/json'
    });
    return this.http.post(url, complaintInfo, { headers: headers });
  }
  // get all complaints
  getComplaintsData(){
    const url = this.baseUrl + '/users/complaints';
    const headers = new HttpHeaders({
      Authorization: `${localStorage.getItem("token")}`,
      'Content-Type': 'application/json'
    });
    return this.http.get(url, { headers: headers });
  }
  
}
