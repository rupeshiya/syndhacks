import { Injectable } from '@angular/core';
import { AuthServiceService } from './auth-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class DataServiceService {
baseUrl = '/api/posts';

  constructor(
    private authService: AuthServiceService,
    private http: HttpClient,
    private router: Router
  ) { }
  loadUserToken(){
    this.authService.loadUserData();
  }

  // get all posts
  getAllPosts(){
    const url = this.baseUrl + '/';
    const headers = new HttpHeaders();
    headers.append("Content-Type", "application/json");
    return this.http.get<any>(url, { headers: headers });
  }

  // get single post by id 
  getSinglePostById(postId){
    const url = this.baseUrl + `/${postId}`;
    let headers = new HttpHeaders({
      Authorization: `${localStorage.getItem("token")}`,
      'Content-Type': 'application/json'
    });
    return this.http.get<any>(url, { headers: headers });
  }
  
  // delete post by id 
  deletePostById(postId){
    const url = this.baseUrl + `/delete/${postId}`;
    let headers = new HttpHeaders({
      Authorization: `${localStorage.getItem("token")}`,
      'Content-Type': 'application/json'
    });
    return this.http.delete(url, { headers: headers });
  }

  // create post 
  createPost(postInfo){
    const url = this.baseUrl + '/create';
    let headers = new HttpHeaders({
      Authorization: `${localStorage.getItem("token")}`,
      'Content-Type': 'application/json'
    });
    return this.http.post<any>(url, postInfo, { headers: headers });
  }

  // comment on the post
  commentOnPost(commentInfo, postId){
    const url = this.baseUrl + `/comment/${postId}`;
    let headers = new HttpHeaders({
      Authorization: `${localStorage.getItem("token")}`,
      'Content-Type': 'application/json'
    });
    return this.http.post<any>(url,commentInfo, { headers: headers });
  }

  // edit post 
  editPost(postId, newInfo){
    const url = this.baseUrl + `/edit/${postId}`;
    let headers = new HttpHeaders({
      Authorization: `${localStorage.getItem("token")}`,
      'Content-Type': 'application/json'
    });
    return this.http.put<any>(url, newInfo, { headers: headers });
  }
  // get user data for dashboard of agent 
  getAgentsDashboardData(){
    const url = this.baseUrl.replace('posts', 'users');
    const newUrl = url + '/current';
    const headers = new HttpHeaders({
      Authorization: `${localStorage.getItem("token")}`,
      'Content-Type': 'application/json'
    });
    return this.http.get<any>(newUrl, { headers: headers });
  }

  // add bonus
  addBonusPoints(bonusInfo){
    const url = '/api/admin/add/bonus';
    const header = new HttpHeaders({
      Authorization: `${localStorage.getItem('token')}`,
      'Content-Type': 'application/json'
    });
    return this.http.post<any>(url, bonusInfo, { headers: header });
  }

  // get bonus points
  getBonusPoints(){
    const url = '/api/users/bonus';
    const header = new HttpHeaders({
      Authorization: `${localStorage.getItem('token')}`,
      'Content-Type': 'application/json'
    });
    return this.http.get<any>(url, { headers: header });
  }
  
}
