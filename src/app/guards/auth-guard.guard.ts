import { Router } from "@angular/router";
import { Injectable } from "@angular/core";
import { CanActivate } from "@angular/router";
import { AuthServiceService } from "../services/auth-service.service";

@Injectable()
export class AuthGuardGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: AuthServiceService
  ) {}

  // function to protect routes
  canActivate() {
    if (this.authService.loggedIn()) {
      return true;
    } else {
      this.router.navigate(["/login"]);
      return false;
    }
  }
}
