const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const Users = require('../models/Users');
const bcrypt = require('bcryptjs');
const keys = require('../config/keys');
const jwt = require('jsonwebtoken');
const passport = require('passport');


// router.get('/complaints', (req, res)=>{
//   console.log('test worked');
//   res.json({success: true, msg: 'test worked'})
// })

// get all complaints
// /api/users/complaints
// private 
router.get('/complaints', passport.authenticate('jwt', { session: false }), (req, res)=>{
  console.log()
  Users.find({})
  .then((users)=>{
    console.log(users);  
    if(users){
      users.forEach((user)=>{
        if(user.adminOrAgent){
          if(user.complaints.length > 0){
            const complaints = user.complaints;
            res.json({success: true, complaints: complaints });
          }
        }
      })  
    } else {
      return res.status(404).json({success: false, msg: 'Admin not found!'});
    }
  })
  .catch((err)=>{
    console.log(err);
     res.status(404).json({success: false, msg: 'Not found' })
  });
})


// get earned points of a user
// /api/users/points
// private
router.get('/bonus',passport.authenticate('jwt',{ session: false }),(req, res)=>{
  // check user points
  Users.findById(req.user.id)
    .then((user)=>{
      if(!user){
        return res.status(404).json({success: false, msg: 'User not found !'});
      } else {
        // check for points earned
        const points = +(user.pointsEarned);
        res.json({success: true, msg: 'Points earned', points: points });
      }
    })
    .catch((err)=>{
      console.log(err);
      res.status(404).json({success: false, msg: 'Not found!'});
    });
});



// register
// /api/users/register
router.post('/register',(req, res)=>{
  // check if user already exists 
  Users.findOne({ email: req.body.email })
    .then((user)=>{
      if(user){
        return res.status(400).json({success: false, msg: 'User already exists '});
      } else {
        // create new users 
        const newUser = new Users({
          email: req.body.email,
          password: req.body.password,
          name: req.body.name,
          adminOrAgent: req.body.adminOrAgent
        });

        // hash the password and save 
        bcrypt.genSalt(10, (err, salt)=>{
          if(err){
            console.log(err);
          }
          bcrypt.hash(newUser.password, salt, (err, hash)=>{
            if(err){
              console.log(err);
            } else {
              newUser.password = hash;
              newUser.save()
                .then((userSaved)=>{
                  res.json({success: true, msg: `${req.body.email} successfully registered `, user: userSaved });
                })
                .catch((err)=>{
                  console.log(err);
                });
            }
          })
        })
      }
    })
    .catch((err)=>{
      res.status(400).json({success: false, msg : err})
    });
});

// routes for login 
// /api/users/login
router.post('/login', (req, res)=>{
  // check if user exists or not 
  const email = req.body.email;
  const password = req.body.password;

  Users.findOne({ email: email })
    .then((user)=>{
      if(!user){
        return res.status(404).json({success: false, msg: `${email} does not exist `});
      } else {
        // authenticate the user 
        bcrypt.compare(password, user.password, (err, success)=>{
          if(err){
            res.status(400).json({success: false, msg:'Password is incorrect!'});
          } 
          if(success){
            // res.json({success: true, msg: 'successfully loggedIn'});
            const payload = { id: user._id, email: user.email };

            // create token
            jwt.sign(payload, keys.secret, {expiresIn: 3600},(err, token)=>{
              res.json({success: true, token: 'Bearer ' + token, admin: user.adminOrAgent });
            })
          }
        })
      }
    })
    .catch((err)=>{
      res.status(400).json({success: false, msg: err});
    });
});

// get current user 
// /api/users/current
// private 

router.get('/current', passport.authenticate('jwt', { session: false}),(req, res)=>{
  // check and send the userInfo 
  Users.findById(req.user.id)
    .then((user)=>{
      if(!user){
        return res.status(404).json({success: false, msg: 'User does not exist' });
      } else {
        res.json({success: true, user: user });
      }
    })
    .catch((err)=>{
      res.status(404).json({success: false ,msg: 'User not found '});
    });
});

// get user by id 
// /api/users/:userId,
// private 
router.get('/:userId', passport.authenticate('jwt', { session: false }),(req, res)=>{
  Users.findById(req.params.userId)
    .then((user)=>{
      if(!user){
        return res.status(404).json({success: false, msg: 'User not found '});
      } else {
        res.json({success: true, user: user });
      }
    })
    .catch((err)=>{
      res.status(404).json({success: false, msg: err});
    })
});

// add complaints 
// /api/users/complaint
// private 
router.post('/complaint', passport.authenticate('jwt', { session: false }), (req, res)=>{
  // user can add multiple complaints 
  const complaint = {
    title: req.body.title,
    description: req.body.description,
    user: req.user.id,
    date: Date.now()
  };
  Users.find({})
  .then((users)=>{
    if(users){
      console.log(users);  
      users.forEach((user)=>{
        if(user.adminOrAgent){
          user.complaints.unshift(complaint)
          user.save()
            .then((userInfo) => {
              console.log(userInfo);
              res.json({
                success: true,
                msg: 'successfully registered the complaints ',
                admin: userInfo
              });
            })
            .catch((err) => {
              console.log(err);
            });
        }
      })  
    }
  })
  .catch((err)=>{
    res.status(404).json({success: false, msg: err })
  });
});

module.exports = router;