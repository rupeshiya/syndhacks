const express = require('express');
const passport = require('passport');
const mongoose = require('mongoose');
const Users = require('../models/Users');
const Posts = require('../models/Posts');
const jwt = require('jsonwebtoken');
const router = express.Router();

// create posts 
// /api/posts/create 
// private 
router.post('/create',passport.authenticate('jwt',{session: false}),(req, res)=>{
  // create posts 
  const newPost = new Posts({
    title: req.body.title,
    description: req.body.description,
    user: req.user.id,
    points: req.body.points,
    date: Date.now()
  });
  // save the post
  newPost.save()
    .then((post)=>{
      res.json({success: true, msg: 'successfully saved ', post: post});
    })
    .catch((err)=>{
      console.log(err.errors);
      res.status(400).json({success: false, msg: err });
    });
});

// edit the post 
// /api/post/edit/:postId
// private 

router.put('/edit/:postId', passport.authenticate('jwt',{session: false}), (req, res)=> {
    // check if post exists or not 
    Posts.findById(req.params.postId)
      .then((post)=>{
        console.log(post);
        if(!post){
          return res.status(404).json({success: false, msg : 'Post not found'});
        } else {
          // update the post 
          post.title = req.body.title;
          post.description = req.body.description;
          post.points = req.body.points;
          post.date = Date.now();
          post.save()
            .then((posts)=>{
              res.json({success: true, msg: 'successfully updated the post '});
            })
            .catch((err)=>{
              console.log(err);
              res.status(400).json({success: false, msg: err});
            });
        }
      })
      .catch((err)=>{
        res.status(400).json({success: false, msg: 'Post not found!'});
      });
});


// delete post 
// /api/posts/delete/:postId
// private 
router.delete('/delete/:postId', passport.authenticate('jwt',{session: false}), (req,res)=>{
  // check if posts exists and remove 
  Posts.findByIdAndRemove(req.params.postId)
    .then((posts)=>{
      res.json({success: true, msg: 'Deleted ', posts})
    })
    .catch((err)=>{
      console.log(err);
      res.status(404).json({success: false,msg: 'Post not found!'});
    });
});

// add comments 
// /api/posts/comment/:postId
// private 
router.post('/comment/:postId',passport.authenticate('jwt',{session: false}),(req, res)=>{
  // if post exists add comment to the post 
  Posts.findById(req.params.postId)
    .then((post)=>{
      // here user can comment multiple times
      const comments = {
        text: req.body.text,
        user: req.user.id,
        points: req.body.points,
        date: Date.now()
      };
      post.comments.unshift(comments);
      post.save()
        .then((post)=>{
          res.json({success: true, msg: 'comment added successfully', post: post});
        })
        .catch((err)=>{
          res.json({success: false ,msg: err})
        });
    })
    .catch((err)=>{
      res.json({success: false, msg: 'Post not found '});
    });
});

// delete comments 
// /api/posts/comment/:postId/:commentId
// private
router.delete('/comment/:postId/:commentId', passport.authenticate('jwt', { session: false }), (req, res)=>{
  Posts.findById(req.params.postId)
    .then((post)=>{
      // check if comment exists 
      const commentIdArray = post.comments.map(comment => comment._id);
      const commentIdToDelete = commentIdArray.indexOf(req.params.commentId);
      post.comments.splice(commentIdToDelete, 1);
      post.save()
        .then((response)=>{
          console.log(response);
          res.json({success: true, msg: 'Comment deleted!', response});
        })
        .catch((err)=>{
          console.log(err);
          res.status(404).json({success: false, msg: 'Unable to delete comment!'});
        });
    });
});

// get all posts  
// /api/posts/
// public 
router.get('/', (req, res)=>{
  Posts.find({})
    .populate('user', ['name', 'email'])
    .exec((err, posts)=>{
      if(!err){
        res.json({success: true, posts: posts });
      }
      if(err){
        res.json({success: false, msg: 'No post found!'});
      }
    })
})

// get single post 
// /api/posts/:postId
// private 
router.get('/:postId', passport.authenticate('jwt',{session: false}), (req, res)=>{
  // find the post and serve 
  Posts.findById(req.params.postId)
    .then((post)=>{
      res.json({success: true, post: post });
    })
    .catch((err)=>{
      res.status(400).json({success: false, msg: 'Post not found '});
    });
});

// get comments
// /api/posts/comments
// private 
// router.get('/comments', passport.authenticate('jwt',{ session: false}), (req, res)=>{
//   Posts.find({})
//     .then((posts)=>{
//       console.log(posts)
    
//     })
// })




module.exports = router;