const express = require('express');
const router = express.Router();
const Users = require('../models/Users');
const Posts = require('../models/Posts');
const passport = require('passport');


// add bonus to the agents
// /api/admin/add/bonus
// private  
router.post('/add/bonus', passport.authenticate('jwt', {session: false}),(req, res)=>{
  // check if user exists 
  const name = req.body.name;
  const bonusAmount = +(req.body.bonus); // converted into a number
 
  Users.findOne({name: name})
    .then((user)=>{
      if(!user){
        return res.status(404).json({success: false, msg: `Agent ${req.body.name} is not found!`});
      } else{
        // if user exists add bonus 
        user.pointsEarned = user.pointsEarned + bonusAmount; 
        user.save()
          .then((user)=>{
            console.log(user);
            res.json({success: true, msg: 'successfully added bonus!'});
          })
          .catch((error)=>{
            console.log(error);
          })
      }
    })
    .catch((err)=>{
      console.log(err);
      res.status(400).json({success: false, msg: `${req.body.name} not found!`});
    });
});



module.exports = router;