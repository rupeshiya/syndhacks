require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const passport = require('passport');
const app = express();
const bodyParser = require('body-parser');
const compression = require('compression');
const morgan = require('morgan');
const PORT = process.env.PORT || 5000;
const { connectMongo } = require('./config/db');
const  users = require('./routes/users');
const posts = require('./routes/posts');
const admin = require('./routes/admin');
const cors = require('cors');
// to disable restriction on unlimited event emitter
process.setMaxListeners(0); 

// middleware 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(compression());
app.use(morgan('combined'));
// static files
app.use(express.static(path.join(__dirname, '/dist/syndHack')));

// passport middleware
app.use(passport.initialize());
// passport config
require('./config/passport')(passport);

// enable cors
app.use(cors());
// connect mongo 
connectMongo();

// routes
app.get('/test', (req, res)=>{
  res.json({success: true, msg: 'test route worked '});
});
app.get('/',(req, res)=>{
  res.sendFile(path.join(__dirname, '/dist/syndHack/index.html'));
});

// users api 
app.use('/api/users',users);
app.use('/api/posts',posts);
app.use('/api/admin',admin);

// listen to the port 
app.listen(PORT, ()=>{
  console.log(`listening on ${PORT}`);
});

module.exports = app;