const mongoose = require('mongoose');
const keys = require('./keys');

module.exports.connectMongo = () => {
  mongoose.connect(keys.mongoUri, { useNewUrlParser: true })
    .then(()=>{
      console.log('mongo connected ');
    })
    .catch((error)=>{
      console.log(error);
    });
}