const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  adminOrAgent:{
    type: Boolean,
    default: false
  },
  pointsEarned:{
    type: Number,
    default: 0
  },
  password: {
    type: String,
    required: true
  },
  complaints:[
    {
      title:{
        type: String
      },
      description:{
        type: String
      },
      user:{
        type: Schema.Types.ObjectId,
        ref: 'users'
      },
      date:{
        type: Date,
        default: Date.now()
      }
    }
  ],
  date: {
    type: Date,
    default: Date.now()
  }
});


module.exports = user = mongoose.model('users', userSchema);