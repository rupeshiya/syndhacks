const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const postSchema = new Schema({
  title:{
    type: String,
    required: true
  },
  points:{
    type: String
  },
  description:{
    type: String,
    required: true
  },
  user:{
    type: Schema.Types.ObjectId,
    ref: 'users'
  },
  comments:[{
    text:{
      type: String,
      required: true
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: 'users'
    },
    date:{
      type: Date,
      default: Date.now()
    }
  }],
  date:{
    type: Date,
    default: Date.now()
  }
})

module.exports = mongoose.model('posts', postSchema);