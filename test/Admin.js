process.env.NODE_ENV = 'dev';
const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const server = require('../app');
const Posts = require('../models/Posts');
const userName = 'Rupeshiya';
const password = 'password';
const email = "test7@test.com";
const userId = '5d737845adecce2ddcd8983a';
const postId = '5d739327a47d723f1fa470ad';
var loggedInToken = '';
const points = 100;

chai.use(chaiHttp);

//  function to add bonus to an agent accounts
function addBonusToAgent(done){
  chai.request(server)
    .post('/api/admin/add/bonus')
    .set('Authorization', loggedInToken)
    .send({
      name: userName,
      bonus: points
    })
    .end((err, response) => {
      // console.log(response.body);
      expect(response.body).to.be.an('object');
      expect(response.body).to.have.property('success');
      done();
    })
}

// grouping tests 
describe('admin tests', ()=>{
  // before all 
  before((done)=>{
    chai.request(server)
    .post('/api/users/login')
    .send({
      email: email,
      password: password
    })
    .end((err, response)=>{
      loggedInToken = response.body.token;
      done();
    });
  });

  it('should add bonus into an agent\'s account', (done)=>{
    addBonusToAgent(done);
  })
})