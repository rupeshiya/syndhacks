process.env.NODE_ENV = 'dev';
const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
const server = require('../app');
const Posts = require('../models/Posts');
const userName = 'Rupeshiya';
const password = 'password';
const email = "test7@test.com";
const userId = '5d737845adecce2ddcd8983a';
const postId = '5d739327a47d723f1fa470ad';
var loggedInToken = '';
const points = 100;
var commentId = '';
const postObj = {
  title: "title",
  description: 'description',
  user: userId,
  points: points,
  date: Date.now()
};

chai.use(chaiHttp);

// to add the post 
function addPost(done){
  chai.request(server)
    .post('/api/posts/create')
    .set('Authorization', loggedInToken)
    .send(postObj)
    .end((err, response) => {
      // console.log(response.body);
      expect(response.body).to.be.an('object')
      expect(response.body).to.have.property('success');
      expect(response.body).to.have.property('msg');
      expect(response.body).to.have.property('post');
      Posts.findByIdAndRemove(response.body.post._id)
        .then(() => {
          done();
        });
    })
}

// func to edit the post /api/post/edit/:postId
function editPost(done){
 chai.request(server)
   .put(` /api/post/edit/${postId}`)
   .set('Authorization', loggedInToken)
   .send(postObj)
   .end((err, response) => {
     // console.log(response.body);
     expect(response.body).to.be.an('object');
     expect(response.body).to.have.property('success');
     expect(response.body).to.include({
       success: true
     });
     done();
   })
}

// function to delete post 
function deletePost(done){
   chai.request(server)
     .delete(`/api/posts/${postId}`)
     .set('Authorization', loggedInToken)
     .end((err, response) => {
       expect(response.body).to.be.an('object');
       expect(response.body).to.include({
         success: true
       });
       expect(response.body).to.have.property('posts');
       done();
     })
}


// func to add the comment  /api/posts/comment/:postId
function addComment(done){
  const commentObj = {
    text: "comment testing",
    user: userId,
    points: points,
    date: Date.now()
  };
  chai.request(server)
    .post(`/api/posts/comment/${postId}`)
    .set('Authorization', loggedInToken)
    .send(commentObj)
    .end((err, response) => {
      commentId = response.body
      // console.log(response.body);
      expect(response.body).to.be.an('object');
      expect(response.body).to.have.property('success')
      expect(response.body).to.include({
        success: true
      });
      // delete comment added
      done();
    })
}

//  function to delete comment
// function deleteComment(done) {
//   chai.request(server)
//     .delete(`/api/post/${postId}/comment/${}`)
// }

//  function to get all the posts 
function getAllPosts(done){
  chai.request(server)
  .get('/api/posts/')
  .end((err, response)=>{
    // console.log(response.body);
    expect(response.body).to.be.an('object');
    expect(response.body).to.have.property('success');
    expect(response.body).to.include({success: true});
    expect(response.body).to.have.property('posts');
    done();
  })
}

//  function to get a single post by id 
function getSinglePostById(done){
  chai.request(server)
    .get(`/api/posts/${postId}`)
    .set('Authorization', loggedInToken)
    .end((err, response) => {
      // console.log(response.body);
      expect(response.body).to.be.an('object');
      expect(response.body).to.have.property('success');
      expect(response.body).to.include({
        success: true
      });
      expect(response.body).to.have.property('post');
      done();
    });
}

// grouping all the tests together
describe('POSTS: ',()=>{

  // before each
  before(function (done) {
    chai.request(server)
      .post('/api/users/login')
      .send({
        email: email,
        password: password
      })
      .end((err, res) => {
        loggedInToken = res.body.token;
        console.log(loggedInToken);
        done();
      })
  })
  
  describe('ADD POST: ', ()=>{
    // add the post 
    it('should create the post: ', (done) => {
      addPost(done);
    });
  })
  

  describe('UPDATE POST: ', () => {
    //edit the post 
    it('should update the post',(done)=>{
      editPost(done);
    });
  });
  
 
  describe('DELETE POST: ', () => {
    // delete post 
    it('should delete the post', (done) => {
      deletePost(done);
    })
  })

  describe('ADD COMMENT: ' , ()=>{
    // comment on a post 
    it('should add a comment on the post', (done) => {
      addComment(done);
    })
  });

  describe('ALL POSTS: ', () => {
     // get all posts 
     it('should give all the posts', (done) => {
       getAllPosts(done);
     })
  })

 

  describe('SINGLE POST: ', () => {
     // get single post by id 
     it('should give single post of a given id', (done) => {
       getSinglePostById(done);
     })
  })
})