process.env.NODE_ENV = 'dev';
const chai = require('chai');
const should = chai.should();
const chaiHttp = require('chai-http');
const server = require('../app');
const Users = require('../models/Users');
const expect = chai.expect;
const userName = 'Rupeshiya';
const password = 'password';
const email = "test9@test.com";
const userId = '5d737845adecce2ddcd8983a';
var loggedInToken = '';
const async = require('async');

chai.use(chaiHttp);

// fn to register a user
function registerUser(done){ 
   const newUser = new Users({
     name: userName,
     password: password,
     email: email,
     adminOrAgent: false
   });
  chai.request(server)
    .post('/api/users/register')
    .send(newUser)
    .end(function(err, res){
      console.log(res.body);
      expect(res.body).to.be.an('object');
      expect(res.body).to.have.property('success');
      expect(res.body).to.deep.include({success: true, msg: `${email} successfully registered `});
      // Users.findByIdAndRemove(res.body.user._id)
      // .then(()=>{
      //   done();
      // });
      done();
    })
}

//  for logging in the users
function loginUser(done){
  chai.request(server)
    .post('/api/users/login')
    .send({
      email: email,
      password: password
    })
    .end(function(err, res){
      // console.log(res.body);
      expect(res.body).to.be.an('object');
      expect(res.body).to.have.property('success');
      expect(res.body).to.have.property('token');
      expect(res.body).to.have.property('admin');
      done();
    })
}

// function to get current user
function getCurrentUser(done){
  chai.request(server)
    .get('/api/users/current')
    .set('Authorization', loggedInToken)
    .end(function (err, res) {
      expect(res.body).to.be.an('object');
      expect(res.body).to.have.property('success');
      expect(res.body).to.have.property('user');
      done();
    })
}

// get user by id
function getUserById(done){
  chai.request(server)
    .get(`/api/users/${userId}`)
    .set('Authorization', loggedInToken)
    .end(function (err, response) {
      // console.log(response.body);
      expect(response.body).to.be.an('object');
      expect(response.body).to.have.property('success');
      expect(response.body).to.have.property('user');
      done();
    });
}

// func to add complaints
function addComplaint(done){
  const commentObj = {
    title: 'testing',
    description: 'description',
    user: userId,
    date: Date.now()
  };
  chai.request(server)
    .post('/api/users/complaint')
    .set('Authorization', loggedInToken)
    .send(commentObj)
    .end((err, response) => {
      // console.log(response.body);
      expect(response.body).to.be.an('object');
      expect(response.body).to.have.property('success');
      expect(response.body).to.have.property('msg');
      expect(response.body).to.have.property('admin');
      done();
    })
}

// test for getting points
function getPoints(done){
   chai.request(server)
     .get('/api/users/bonus')
     .set('Authorization', loggedInToken)
     .end((err, response) => {
       expect(response.body).to.be.an('object');
       expect(response.body).to.have.property('success');
       expect(response.body).to.have.property('points');
       done();
     })
}

// get all the complaints 
function getComplaints(done){
  chai.request(server)
    .get('/api/users/complaints')
    .set('Authorization', loggedInToken)
    .end((err, response) => {
      // console.log(response.body);
      expect(response.body).to.be.an('object');
      expect(response.body).to.have.property('success');
      expect(response.body).to.have.property('complaints')
      done();
    })
}


// grouping test cases
describe('users-',()=>{

  // before all
  before(function(done){
    chai.request(server)
      .post('/api/users/login')
      .send({
        email: email,
        password: password
      })
      .end((err, res)=>{
        loggedInToken = res.body.token;
        console.log(loggedInToken);
        done();
      })
  })

  // create a user before each test case
  // beforeEach(function(done){
  
  //   // create a new user
  //   const newUser = new Users({
  //     name: userName,
  //     password: password,
  //     email: email,
  //     adminOrAgent: false
  //   });
  
  //   newUser.save()
  //     .then(()=>{
  //       done();
  //     });
  // });

  // remove the created user after the test case
  // afterEach(function(done){
  //   Users.findOneAndRemove({name: userName})
  //     .then(()=>{
  //       done();
  //     });
  // });

  describe('REGISTER /api/users/register : ', () => {
    // to register the user
    it('should register the users', (done) => {
      registerUser(done);
    });
  });

  describe('LOGIN /api/users/login ', ()=>{
    // to login the user
    it('should login the user', (done)=>{
      loginUser(done);
    });
  });

  describe('CURRENT LOGGEDIN USER: /api/users/current', ()=>{
    // to get the current logged in user
    it('should give the current logged in user', (done) => {
      getCurrentUser(done);
    });
  });

  describe('USER of a id /api/users/userID: ', ()=>{
    // to get user by userId
    it('should give the user of particular id ', (done) => {
      getUserById(done);
    });
  });

  describe('ADD COMPLAINTS /api/users/complaint ', ()=>{
    // test for add complaints
    it('should add complaint ', (done) => {
      addComplaint(done);
    });
  });

  describe('GET POINTS: /api/users/points - ', ()=>{
    // get all the points earned by a user
    it('should give the points earned by a user', (done) => {
      getPoints(done);
    });
  });

  describe('GET ALL COMPLAINTS : /api/users/complaints ', ()=>{
    // get all the complaints
    it('should give all the complaints', (done) => {
      getComplaints(done);
    });
  })
});