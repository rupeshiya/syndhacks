const puppeteer = require('puppeteer');
let page;
let browser;

async function addAdmin(done){
  await page.click('span.navbar-toggler-icon');
  // set the token to localStorage
  await page.evaluate(() => {
    localStorage.setItem('token', JSON.stringify('Bearer Hello_world'));
    console.log('localStorage: - ', JSON.parse(localStorage.getItem('token')));
  });
  const register = await page.$eval('a[href="#/register"]', el => el.innerHTML);
  expect(register).toEqual('Register');
  await page.click('a[href="#/register"]');
  const url = await page.url(); // gives the url of the page
  console.log(url);
  expect(url).toEqual('http://localhost:5000/#/register');
  done();
}

async function adminDashboard(done){
  await page.click('span.navbar-toggler-icon');
  // set the token to localStorage
  await page.evaluate(() => {
    localStorage.setItem('token', JSON.stringify('Bearer Hello_world'));
    console.log('localStorage: - ', JSON.parse(localStorage.getItem('token')));
  });

  // wait for DOM
  // await page.waitFor('a[href="#/admin-dashboard"]');

  // not visible since chromium is fast to run so not visible 
  const adminDashboard = await page.$eval('a[href="#/admin-dashboard"]', el => el.innerHTML);
  expect(adminDashboard).toEqual('Admin Dashboard');
  await page.click('a[href="#/admin-dashboard');
  const url = await page.url(); // gives the url of the page
  console.log(url);
  expect(url).toEqual('http://localhost:5000/#/admin-dashboard');
  done();
}

async function goToDashboard(done){
  await page.goto('http://localhost:5000/#/home');
  // set the token to localStorage
  await page.evaluate(() => {
    localStorage.setItem('token', JSON.stringify('Bearer Hello_world'));
    console.log('localStorage: - ', JSON.parse(localStorage.getItem('token')));
  });

  await page.click('span.navbar-toggler-icon');
  const profile = await page.$eval('ul>li>a#navbarDropdown.nav-link.dropdown-toggle', el => el.innerHTML);
  console.log(profile);
  expect(profile).toEqual('Profile');

  await page.click('#navbarDropdown.nav-link.dropdown-toggle')[1];

  const dashboard = await page.$eval('a[href="#/dashboard"]', el => el.innerHTML);
  expect(dashboard).toEqual('Dashboard');
  await page.click('a[href="#/dashboard"]');
  const url = page.url();
  expect(url).toEqual('http://localhost:5000/#/dashboard');
  done();
}

describe('ADMIN TESTINGS :', ()=>{
  beforeEach(async () => {
    jest.setTimeout(30000);
    // launching browser
    browser = await puppeteer.launch({
      headless: true,
      args: ['--no-sandbox']
    });
    page = await browser.newPage();
    await page.goto('http://localhost:5000');
  })

  afterEach(async () => {
    await browser.close();
  });
  //  go to admin dashboard
  test('should go to admin dashboard', (done) => {
    adminDashboard(done);
  });

  // go to add admin page
  test('should go to add admin page', (done) => {
    addAdmin(done);
  });

  // go to dashboard
  test('should go to dashboard page: ', (done) => {
    goToDashboard(done);
  });

})