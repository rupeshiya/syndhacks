const puppeteer = require('puppeteer');
let page;
let browser;

async function loginUser(done) {

  await page.goto('http://localhost:5000/#/login');
   // click email input field and type email to login
   await page.type('input#email', 'devilwebdev@gmail.com', {
     delay: 100
   });

   //  click on input field of password and enter the password
   await page.type('input#password', 'abc123', {
     delay: 100
   });

   // now click on login button to login 
   await page.click('button#loginButton');
   const url = await page.url();
   console.log(url);
   expect(url).toEqual('http://localhost:5000/#/login');
   done();
}

async function registerUser(done){
    await page.goto('http://localhost:5000/#/register');
    await page.type('input#name', 'test5', { delay: 100});
    await page.type('input#email', 'test5@test.com', { delay: 100});
    await page.type('input#password', 'test@123', { delay: 100});
    const option1 = await page.select('select#exampleFormControlSelect1', "false");
    console.log(option1);
    await page.click('button.btn.btn-primary');
    const url = page.url();
    expect(url).toEqual('http://localhost:5000/#/login');
    done();
}

async function navLoginButton(done){
   await page.click('span.navbar-toggler-icon');
   const login = await page.$eval('a[href="#/login"]', el => el.innerHTML);
   expect(login).toEqual('Login');
   await page.click('a[href="#/login"]');
   const url = await page.url(); // gives the url of the page
   console.log(url);
   expect(url).toEqual('http://localhost:5000/#/login');
   done();
}

async function navRegisterButton(done){
   await page.click('span.navbar-toggler-icon');
  const register = await page.$eval('a[href="#/register"]', el => el.innerHTML);
  expect(register).toEqual('Register');
  await page.click('a[href="#/register"]');
  const url = await page.url(); // gives the url of the page
  console.log(url);
  expect(url).toEqual('http://localhost:5000/#/register');
  done();
}

describe('AUTH TESTINGS :', ()=>{
  
  beforeEach(async () => {
    jest.setTimeout(10000);
    // launching browser
    browser = await puppeteer.launch({
      headless: false,
      args: ['--no-sandbox']
    });
    page = await browser.newPage();
    await page.goto('http://localhost:5000');
  })

  afterEach(async ()=>{
    await browser.close();
  });

   test('should give login page: ', async (done) => {
     navLoginButton(done);
   });

   test('should give register page: ', async (done) => {
     navRegisterButton(done);
   });

   // should register
   test('should register the user: ', async (done) => {
     registerUser(done);
   });

   // test the login 
   test('should add the credential and login :', async (done) => {
     loginUser(done);
   });

   //  should logout the user
   test('should logout the user: ', async () => {
     // set the token to localStorage
     await page.evaluate(() => {
       localStorage.setItem('token', JSON.stringify('Bearer Hello_world'));
       console.log('localStorage: - ', JSON.parse(localStorage.getItem('token')));
     });

     await page.goto('http://localhost:5000/#/posts');
     const links = await page.click('ul>li>a.nav-link');
     console.log(links);
   });
});
