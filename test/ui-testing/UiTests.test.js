const puppeteer = require('puppeteer');
let page;
let browser;

describe('UI TESTINGS :', ()=>{
 
  beforeEach(async () => {
    jest.setTimeout(30000);
    // launching browser
    browser = await puppeteer.launch({
      headless: false,
      args: ['--no-sandbox']
    });

    // creating a new tab 
    page = await browser.newPage();
    // await page.waitForNavigation({ waitUntil: 'domcontentloaded' });
    await page.goto('http://localhost:5000');
  });

  afterEach(async () => {
    await browser.close();
  });

  describe('#UI TESTING: ', ()=>{
    
    test('should give brand name: ',async ()=> {
      const brandText = await page.$eval('a.navbar-brand', el => el.innerHTML);
      expect(brandText).toEqual('LOGO');
    });

    test('should give register button: ',async ()=>{
      const registerButtonText = await page.$eval('button#registerButton', el => el.innerHTML);
      expect(registerButtonText).toEqual('Register');
    });

    test('should give navigate to register page: ',async () => {
      await page.click('button#registerButton');
      const url = await page.url(); // gives the url of the page
      expect(url).toEqual('http://localhost:5000/#/register');
    });
  });
});

// module.exports = page;
